import React, {useEffect} from 'react';
import { View, Text, Image, StyleSheet} from 'react-native';
import Gambar from '../../assets/splash.png';


const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('WelcomeAuth')
        }, 4000)
    })
    return (
        <View style={styles.margin}>
            <Image source={Gambar}style={styles.gambar}></Image>
            <Text style={styles.teks}>Splash Android</Text>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    margin : {
        justifyContent : 'center',
        alignItems : 'center',
        flex: 1,
    },
    gambar : {
        width:360,
        height:200,
    },
    teks : {
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5,
        textAlign:'center'
    }
});