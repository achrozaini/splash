import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import Home from '../../assets/homepage.png';

const WelcomeAuth = () => {
    return(
        <View style={styles.margin}>
             <Image source={Home} style={styles.home}></Image>
        </View>
    )
}

export default WelcomeAuth;

const styles = StyleSheet.create({
    margin : {
        justifyContent : 'center',
        alignItems : 'center',
        flex : 1,
    },
    home : {
        width:410,
        height:909,
        marginLeft:0
    },
});